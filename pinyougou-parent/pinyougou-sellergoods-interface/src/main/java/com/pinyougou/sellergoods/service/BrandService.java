package com.pinyougou.sellergoods.service;


import com.pinyougou.pojo.TbBrand;
import entity.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 品牌列表
 */
public interface BrandService {

    public List<TbBrand> findAll();

    /**
     * 品牌分页
     * @param pageNum   当前页面
     * @param pageSize  每页记录数
     * @return
     */
    public PageResult findPage(int pageNum,int pageSize);


    /**
     * 增加
     */
    public void add(TbBrand tbBrand);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    public TbBrand findOne(long id);

    /**
     * 修改
     * @param tbBrand
     */
    public void update(TbBrand tbBrand);

    /**
     * 删除
     * @param ids
     */
    public void delete(long[] ids);


    /**
     * 品牌分页
     * @param tbBrand
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageResult findPage(TbBrand tbBrand, int pageNum,int pageSize);


    /**
     * 返回下拉列表
     * @return
     */
    public List<Map> selectOptionList();

}
